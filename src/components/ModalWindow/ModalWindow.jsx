import PropTypes from 'prop-types';
import classes from './modalWindow.module.scss';
import locale from "../../common/locales/en-US.js";

export const ModalWindow = ({ setModalWindowState, imageUrl}) => (
  <div className={classes.modalWindow}>
    <img src={imageUrl} alt={locale.fullPhoto} />
    <button onClick={() => setModalWindowState(false)}>{locale.close}</button>
  </div>
);

ModalWindow.propTypes = {
  setModalWindowState: PropTypes.func.isRequired,
  imageUrl: PropTypes.string.isRequired 
};