import { useState } from "react";
import PropTypes from 'prop-types';
import classes from './searchBar.module.scss';
import locale from "../../common/locales/en-US.js";

export const SearchBar = ({ dataList, setFilteredList }) => {

  const [searchInputValue, setSearchInputValue] = useState('');

  const onChangeInputHandler = (inputValue) => {
    setSearchInputValue(inputValue);
  };

  const onClickSearchHandler = () => {

    const filteredData = [];

    dataList.map((e) => {
      if (e.albumId === parseInt(searchInputValue)) {
        filteredData.push(e);
      }
    });

    setFilteredList(filteredData);
  };

  return (
    <div className={classes.searchBar}>
      <input
        type="text"
        className={classes.searchBar__input}
        placeholder={locale.enterAlbumId}
        onChange={e => onChangeInputHandler(e.target.value)}
      />
      <button className={classes.searchBar__button} onClick={onClickSearchHandler}>Search</button>
    </div>
  );
};

SearchBar.propTypes = {
  dataList: PropTypes.arrayOf(
    PropTypes.shape({
      albumId: PropTypes.number,
      id: PropTypes.number,
      thumbnailUrl: PropTypes.string,
      title: PropTypes.string,
      url: PropTypes.string
    })
  ).isRequired,
  setFilteredList: PropTypes.func.isRequired
};