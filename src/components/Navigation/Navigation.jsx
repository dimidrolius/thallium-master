import PropTypes from 'prop-types';
import classes from "./navigation.module.scss";
import { SearchBar } from "../SearchBar/SearchBar";
import locale from "../../common/locales/en-US.js";

export const Navigation = ({ dataList, setDataList, filteredList, setFilteredList }) => (
  <nav className={classes.nav}>
    <h1>{locale.pageTitle}</h1>
    <SearchBar
      dataList={dataList}
      setDataList={setDataList}
      filteredList={filteredList}
      setFilteredList={setFilteredList}
    />
  </nav>
);

Navigation.propTypes = {
  dataList: PropTypes.arrayOf(
    PropTypes.shape({
      albumId: PropTypes.number,
      id: PropTypes.number,
      thumbnailUrl: PropTypes.string,
      title: PropTypes.string,
      url: PropTypes.string
    })
  ).isRequired,
  setDataList: PropTypes.func.isRequired,
  filteredList: PropTypes.arrayOf(
    PropTypes.shape({
      albumId: PropTypes.number,
      id: PropTypes.number,
      thumbnailUrl: PropTypes.string,
      title: PropTypes.string,
      url: PropTypes.string
    })
  ).isRequired,
  setFilteredList: PropTypes.func.isRequired 
};